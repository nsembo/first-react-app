import React from 'react';

class Main extends React.Component {
    render(){
        return (
            <div className="main-body">
                <p className="para">Welcome</p>
                <form className="form">
                    <label for="firstName">First Name: </label>
                    <input type="text" id="firstName" required />

                    <label for="lastName">Last Name: </label>
                    <input type="text" id="lastName" required />

                    <button type="submit" value="submit">Submit</button>
                </form>
            </div>
        );
    }
}

export default Main;